﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paginator : MonoBehaviour {

    [Header("Settings")]
    public int pageSize = 8;
    public int offset = 0;
    public Vector3 spacing;
    public List<GameObject> foodItems;


	// Use this for initialization
	void Start () {
		
	}

    public void SetPage(int pageIndex) {
        if (pageIndex < 0) {
            Debug.LogError("Page index can't be less than 0");
            return;
        }
        if (pageIndex > Mathf.Floor(foodItems.Count/pageSize)) {
            Debug.LogError("Page index can't be bigger than the number of pages");
            return;
        }
        offset = pageIndex * pageSize;
    }

	// Update is called once per frame
	void Update () {
        for (int index = 0; index < foodItems.Count; index++) {
            GameObject foodItem = foodItems[index];
            foodItem.SetActive(false);
        }
		Vector3 basePosition = GetComponent<Transform>().position;
        int startOffset = offset; // 8
        int endOffset = Mathf.Min(offset+pageSize, foodItems.Count); //16
        for (int index = startOffset; index < endOffset; index++) {
            GameObject foodItem = foodItems[index];
            foodItem.SetActive(true);
            //index = 9
            Vector3 position = basePosition + spacing * (index - startOffset);
            foodItem.GetComponent<Transform>().position = position;
        }
	}


}
