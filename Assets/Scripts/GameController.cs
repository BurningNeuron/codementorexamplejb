﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    [Header("Game Settings")]
    [SerializeField] List<LevelData> levels;
    [SerializeField] int currentLevel = 0;


    [Header("Wired Components")]
    [SerializeField] Transform[] spawnPoints = new Transform[3];
    Text speechBubble;


    static private GameController instance;

    static public GameController Instance { 
        get {
            return instance;
        }
    }

    public int Tracks {
        get {
            return spawnPoints.Length;
        }
    }

	// Use this for initialization
	void Start () {
        instance = this;
		StartCoroutine(AutoSpawner());
	}

    public void SetSpeechBubbleText(string bubbleText) {
        if (speechBubble != null) {
            speechBubble.text = bubbleText;
        }

    }

	// Update is called once per frame
	void Update () {

	}

    IEnumerator AutoSpawner() {
        while(true) { //You could have some real logic here ;)
            float waitTime = levels[currentLevel].spawnRate;
            yield return new WaitForSeconds(waitTime);
            SpawnFood();
        }
    }

    void SpawnFood() {
        int spawnIndex = Random.Range(0, spawnPoints.Length);

        Transform spawnPosition = spawnPoints[spawnIndex];

        LevelData level = levels[currentLevel];
        GameObject foodObject = level.CreateFoodObject();
        foodObject.GetComponent<FoodItem>().trackIndex = spawnIndex;
        foodObject.transform.position = spawnPosition.position;
    }

    public float TrackPosition(int trackIndex) {
        return spawnPoints[trackIndex].position.y;
    }

}
