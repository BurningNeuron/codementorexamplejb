﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FoodCategory {
    Go,
    Slow,
    Whoa
}

[CreateAssetMenu(fileName = "Food Data", menuName = "My Game/Food Data")]
public class FoodData : ScriptableObject {

    public string foodName = "Some Food";
    public string foodDescription = "A descriptive food item";
    public FoodCategory category = FoodCategory.Go;
    public int pointValue = 1;
    public Sprite sprite;
    public int trackIndex = 0;

    public GameObject CreateFoodObject(GameObject prefab) {
        GameObject foodObject = Instantiate(prefab);
        foodObject.GetComponent<FoodItem>().foodData = this;
        return foodObject;
    }
}
