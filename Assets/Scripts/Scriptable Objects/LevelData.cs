﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level Data", menuName = "My Game/Level Data")]
public class LevelData : ScriptableObject {

    [Header("Settings")]
    public string levelName;
    public string levelDescription;
    public List<FoodData> foodItems;
    public float spawnRate = 5.0f;

    [Header("Wired Components")]
    [SerializeField] GameObject foodItemPrefab; //You might want to use different prefabs for levels. If not it might be easier to have a Singleton somewhere than can be used to hold the reference

    public GameObject CreateFoodObject() {
        if (foodItems.Count == 0) {
            Debug.LogError("You need some food items in the level!");
            return null;
        }

        int itemIndex = Random.Range(0, foodItems.Count);
        FoodData foodData = foodItems[itemIndex];
        GameObject foodObject = foodData.CreateFoodObject(foodItemPrefab);
        return foodObject;
    }
}
