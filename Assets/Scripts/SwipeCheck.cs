﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Define our directions
public enum SwipeDirection {
    None = 0,
    Up = 1,
    Down = 2,
    Left = 4,
    Right = 8
}

//Make a special event so we can put this script on anything and then
//allow any other script to listen for the even and not be tied directly to the swipe script.
//Our custom event will send the SwipDirection and the source (GameObject)
//You will need a collider on the same object for the OnMouseUp and OnMouseDown events to work
[System.Serializable]
public class SwipeEvent : UnityEvent<SwipeDirection,GameObject> {};

public class SwipeCheck : MonoBehaviour {

    enum SwipeType {
        MouseUp,
        MouseDrag
    }

    //The resistance is so small moves don't count, tweak to your satisfaction
    //With the default settings, it is harder to swipe in the Y direction than the X
    [Header("Settings")]
    [SerializeField]float swipeResistanceX = 50.0f;
    [SerializeField] float swipeResistanceY = 100.0f;
    [SerializeField] SwipeType swipeType = SwipeType.MouseUp;

    //The event allows other scripts to easily listen for swipes
    [Header("Event Emitters")]
    [SerializeField] SwipeEvent swipeEvent;


    Vector3 downPosition;

    bool dragBegun = false;

    void OnMouseDown() {
        //This method is triggered when the mouse is down on the object (provided it has a collider)
        //We save the position the mouse (or touch) was started
        downPosition = Input.mousePosition;
        dragBegun = true;
    }

    void OnMouseUp() {

        if (swipeType != SwipeType.MouseUp) {
            return;
        }

        //Triggered after a release from the mouse coming down on the object. It doesn't matter if
        //the mouse (or touch) is over this object, another or nothing

        SwipeDirection direction = GetSwipeDirection();

        //If there is a direction, then we emit the event
        if (direction != SwipeDirection.None) {
            Debug.Log("Swipe Object: "+name);
            Debug.Log("Swipe Direction: "+direction);
            //We provide the direction of the swipe, and the GameObject the swipe is for
            swipeEvent.Invoke(direction, gameObject);
        }
    }

    void OnMouseDrag() {
        if (!(swipeType == SwipeType.MouseDrag && dragBegun)) {
            return;
        }


        SwipeDirection direction = GetSwipeDirection();
        //If there is a direction, then we emit the event
        if (direction != SwipeDirection.None) {
            Debug.Log("Swipe Object: "+name);
            Debug.Log("Swipe Direction: "+direction);
            //We provide the direction of the swipe, and the GameObject the swipe is for
            dragBegun = false;
            swipeEvent.Invoke(direction, gameObject);
        }

    }

    SwipeDirection GetSwipeDirection() {
        //We calculate the distance moved
        Vector2 swipeDelta = downPosition - Input.mousePosition;

        //Default direction is none
        SwipeDirection direction = SwipeDirection.None;

        //See if we overcome the resistance on the X axis
        if (Mathf.Abs(swipeDelta.x) > swipeResistanceX) {
            //And set the direction found
            direction |= (swipeDelta.x < 0) ? SwipeDirection.Right : SwipeDirection.Left;
        }

        //See if we overcome the resistance on the Y axis
        if (Mathf.Abs(swipeDelta.y) > swipeResistanceY) {
            //And set the direction found
            direction |= (swipeDelta.y < 0) ? SwipeDirection.Up : SwipeDirection.Down;
        }

        return direction;
    }
}
