﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodItem : MonoBehaviour {

    [Header("Settings")]
    [SerializeField] Vector2 speed = new Vector2(1,1);
    public int trackIndex = 0;
    public string description;

    [Header("Debug")]
    [SerializeField] FoodData _foodData; //This will be exposed on the inspector, but should only be used for viewing

    [Header("Wired Compoments")] //Any scripts or objects that need to be dragged and referenced can be put here
    [SerializeField] SpriteRenderer spriteRenderer;

    public FoodData foodData {
        get {
            return _foodData;
        }
        set {
            _foodData = value;
            if (_foodData != null) {
                spriteRenderer.sprite = _foodData.sprite;
            }
        }
    }

    void Reset() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

	// Use this for initialization
	void Start () {
		foodData = _foodData;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 nextPosition = transform.position;
        nextPosition.x += Time.deltaTime*speed.x;
		transform.position = nextPosition;
	}

    public void HandleSwipe(SwipeDirection direction, GameObject source) {
        //We don't need to worry about source here, we are just going to assume we are hadling ourselves.
        if (direction == SwipeDirection.Down) {
            SwipeDown();
        }
        if (direction == SwipeDirection.Up) {
            SwipeUp();
        }
    }

    void SwipeDown() {
        Debug.Log("Going Down");

        int newTrack = Mathf.Clamp(trackIndex+1, 0, GameController.Instance.Tracks-1);
        if (newTrack != trackIndex) {
            trackIndex = newTrack;
            StopAllCoroutines();
            StartCoroutine(MoveToTrack());
        }

    }

    void SwipeUp() {
        Debug.Log("Going Up");

        int newTrack = Mathf.Clamp(trackIndex-1, 0, GameController.Instance.Tracks-1);
        if (newTrack != trackIndex) {
            trackIndex = newTrack;
            StopAllCoroutines();
            StartCoroutine(MoveToTrack());
        }

    }

    IEnumerator MoveToTrack() {
        float newY = GameController.Instance.TrackPosition(trackIndex);
        float currentY = transform.position.y;
        float currentTime = 0;

        while (currentTime < 1) {
            Vector3 newPosition = transform.position;
            newPosition.y = Mathf.Lerp(currentY, newY, currentTime);
            transform.position = newPosition;
            currentTime += Time.deltaTime * speed.y;
            yield return null;
        }
        Vector3 finalPosition = transform.position;
        finalPosition.y = newY;
        transform.position = finalPosition;
    }

    void OnMouseUpAsButton() {
        string newDescription = gameObject.name + " " + description;

        GameController.Instance.SetSpeechBubbleText(newDescription);
    }
}
