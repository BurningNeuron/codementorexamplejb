Example For Jesse
=================

Examples to show:

* Using ScriptableObject assets to store config data
* Object touch/swipe handling
* Changing sprites based on config data
* Using ScriptableObject assets to store level data
* Using ScriptableObject assets to store item data
* Custom events


By Michael McHugh (michael@burningneuorn.io)
* https://www.codementor.io/burningneuron
* https://clarity.fm/burningneuron
* https://linked.in/in/mrmchugh
* https://twitter.com/BurningNeuronIO


